CREATE DATABASE hcg_db;
\connect hcg_db;

CREATE TABLE IF NOT EXISTS feedback (
    id SERIAL PRIMARY KEY,
    username VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    language VARCHAR(15) NOT NULL,
    message VARCHAR(150) NOT NULL
);

CREATE TABLE IF NOT EXISTS logs (
    id SERIAL PRIMARY KEY,
    endpoint VARCHAR(255),
    method VARCHAR(10),
    duration FLOAT,
    status_code INTEGER,
    data JSONB,  -- Utilisez JSONB pour stocker les données JSON
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);