import pandas as pd
import uuid
from io import BytesIO

def process_interactive_video(data_file, data, videoTime, decale_nombres_egaux, find_previous_duration, strict=False, autoContinue=True, allowMoveBackward=True, enableSolutionsButton=True, requireCompletion=True):
    if data_file and hasattr(data_file, 'filename'):
        # Lire le fichier CSV ou Excel
        if data_file.filename.endswith('.csv'):
            df = pd.read_csv(BytesIO(data_file.read()))
        elif data_file.filename.endswith('.xlsx'):
            df = pd.read_excel(BytesIO(data_file.read()))
        else:
            raise ValueError("Unsupported file format. Please provide a CSV or Excel file.")

        num_questions = len(df)
        total_video_duration = videoTime

        # Durées
        durations_list = [float(df.at[i, 'duration']) if not pd.isnull(df.at[i, 'duration']) else total_video_duration for i in range(num_questions)]
        sorted_durations = sorted(durations_list)
        durations_list = decale_nombres_egaux(durations_list)

        # Renvois
        seekTo_list = [float(df.at[i, 'renvoi']) for i in range(len(df)) if not pd.isnull(df.at[i, 'renvoi'])]

        # Réinitialiser les interactions dans content.json
        data['interactiveVideo']['assets']['interactions'] = []

        # Ajouter les interactions pour chaque question
        for i in range(num_questions):
            interaction_uuid = uuid.uuid4().hex
            answers = []

            # Préparer les réponses
            for col in df.columns:
                if col.startswith('reponse') and not pd.isnull(df.at[i, col]):
                    result_col = 'result' + col[-1]
                    correct = df.at[i, result_col] == 'v'
                    answers.append({
                        "text": f"<div>{df.at[i, col]}</div>\n",
                        "correct": correct,
                        "tipsAndFeedback": {
                            "tip": "",
                            "chosenFeedback": "",
                            "notChosenFeedback": ""
                        }
                    })

            duration = durations_list[i]

            # Calculer seekTo si nécessaire
            if not seekTo_list:
                if strict:
                    seekTo = find_previous_duration(sorted_durations, duration) + 1
            else:
                seekTo = seekTo_list[i] if i < len(seekTo_list) else None

            # Configurer les données de l'interaction
            interaction_data = {
                "x": 0,
                "y": 0,
                "width": 40,
                "height": 22.5,
                "duration": {
                    "from": duration,
                    "to": duration
                },
                "libraryTitle": "Multiple Choice",
                "action": {
                    "library": "H5P.MultiChoice 1.16",
                    "params": {
                        "media": {"disableImageZooming": False},
                        "answers": answers,
                        "overallFeedback": [{"from": 0, "to": 100}],
                        "behaviour": {
                            "autoContinue": autoContinue,
                            'allowMoveBackward': allowMoveBackward,
                            "timeoutCorrect": 2000,
                            "timeoutWrong": 3000,
                            "soundEffectsEnabled": True,
                            "enableRetry": True,
                            "enableSolutionsButton": enableSolutionsButton,
                            "passPercentage": 100
                        },
                        "UI": {
                            "checkAnswerButton": "Check",
                            "submitAnswerButton": "Submit",
                            "showSolutionButton": "Show solution",
                            "tryAgainButton": "Retry",
                            "tipsLabel": "Show tip",
                            "scoreBarLabel": "You got @num out of @total points",
                            "tipAvailable": "Tip available",
                            "feedbackAvailable": "Feedback available",
                            "readFeedback": "Read feedback",
                            "wrongAnswer": "Wrong answer",
                            "correctAnswer": "Correct answer",
                            "shouldCheck": "Should have been checked",
                            "shouldNotCheck": "Should not have been checked",
                            "noInput": "Please answer before viewing the solution",
                            "a11yCheck": "Check the answers. The responses will be marked as correct, incorrect, or unanswered.",
                            "a11yShowSolution": "Show the solution. The task will be marked with its correct solution.",
                            "a11yRetry": "Retry the task. Reset all responses and start the task over again."
                        },
                        "confirmCheck": {
                            "header": "Finish?",
                            "body": "Are you sure you wish to finish?",
                            "cancelLabel": "Cancel",
                            "confirmLabel": "Finish"
                        },
                        "confirmRetry": {
                            "header": "Retry?",
                            "body": "Are you sure you wish to retry?",
                            "cancelLabel": "Cancel",
                            "confirmLabel": "Confirm"
                        },
                        "question": f"<p>{df.at[i, 'question']}</p>\n"
                    },
                    "subContentId": interaction_uuid,
                    "metadata": {"contentType": "Multiple Choice", "license": "U", "title": "Untitled Multiple Choice"}
                },
                "pause": True,
                "displayType": "poster",
                "buttonOnMobile": False,
                "adaptivity": {
                    "correct": {"allowOptOut": False, "message": "Bonne réponse !"},
                    "wrong": {"allowOptOut": False, "message": "<p>La réponse est incorrecte !</p>\n", "allowMoveBackward": True},
                    "requireCompletion": requireCompletion
                },
                "label": ""
            }

            # Inclure seekTo si nécessaire
            if seekTo_list:
                interaction_data['adaptivity']['wrong']['seekTo'] = seekTo

            # Ajouter l'interaction
            data['interactiveVideo']['assets']['interactions'].append(interaction_data)
    else:
        data['interactiveVideo']['assets']['interactions'] = []