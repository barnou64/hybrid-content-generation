import os
os.environ["IMAGEIO_ffmpeg_EXE"] = "/usr/bin/ffmpeg"
import json
import zipfile
import subprocess
from moviepy.editor import VideoFileClip
import whisper
import pandas as pd
import requests
import shutil
import re

class WhisperModel:
    _model = None

    @classmethod
    def get_model(cls, model_path):
        if cls._model is None:
            cls._model = whisper.load_model(model_path)
        return cls._model

def translate_text(text, source_lang, target_lang):
    url = "http://libretranslate:5000/translate"
    payload = {
        "q": text,
        "source": source_lang,
        "target": target_lang,
        "format": "text",
    }
    headers = {
        "Content-Type": "application/json"
    }
    
    try:
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        translated_text = response.json().get('translatedText', '')
        return translated_text
    except Exception as e:
        return str(e)
    
def detect_language(text):
    response = requests.post("http://libretranslate:5000/detect", data={"q": text})
    return response.json()[0]['language']

def load_data_file(file_path):
    try:
        if file_path.endswith('.csv'):
            data = pd.read_csv(file_path)
        elif file_path.endswith('.xlsx'):
            data = pd.read_excel(file_path)
        else:
            raise ValueError("Unsupported file format. Please provide a CSV or Excel file.")
        return data
    except Exception as e:
        raise ValueError(f"Error loading data file: {str(e)}")

def video_speech_to_text(audio_path: str, model_path: str) -> str:
    try:
        model = WhisperModel.get_model(model_path)
        result = model.transcribe(audio_path)
        return result["text"]
    except Exception as e:
        return str(e)

def modify_video_restrictions(fichier_json, valeur):
    try:
        # Lecture du fichier JSON
        with open(fichier_json, 'r') as f:
            contenu = json.load(f)
        
        # Vérification et modification de la section 'override'
        if 'override' not in contenu:
            contenu['override'] = {}  # Crée l'objet si absent

        contenu['override']['preventSkippingMode'] = 'both' if valeur else 'none'
        
        # Écriture des modifications dans le fichier JSON
        with open(fichier_json, 'w') as f:
            json.dump(contenu, f, indent=4)
    
    except FileNotFoundError:
        print(f"Erreur : le fichier {fichier_json} n'a pas été trouvé.")
    except json.JSONDecodeError:
        print(f"Erreur : le fichier {fichier_json} contient un JSON invalide.")
    except Exception as e:
        print(f"Une erreur inattendue s'est produite : {e}")

def zip_files_in_folder_bash(zip_path):
    subprocess.run(['zip', '-r', zip_path, '.'], cwd='./H5P')

def zip_files_in_folder(zip_path, folder_path):
    with zipfile.ZipFile(zip_path, 'w', zipfile.ZIP_DEFLATED) as zipf:
        for root, dirs, files in os.walk(folder_path):
            for file in files:
                file_path = os.path.join(root, file)
                rel_path = os.path.relpath(file_path, folder_path)
                zipf.write(file_path, arcname=rel_path)
    return zip_path

def video_duration(filename):
    try:
        result = subprocess.run(
            ["ffprobe", "-v", "error", "-show_entries", "format=duration",
             "-of", "default=noprint_wrappers=1:nokey=1", filename],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            check=True
        )
        return float(result.stdout)
    except subprocess.CalledProcessError as e:
        raise
    except FileNotFoundError:
        raise

def extract_audio(video_path, audio_path):
    video = VideoFileClip(video_path)
    audio = video.audio
    audio.write_audiofile(audio_path)
    video.close()

def update_video_path(json_file_path, video_url=None, user_h5p_folder=None):
    with open(json_file_path, 'r') as f:
        data = json.load(f)
    
    if video_url:
        youtube_regex = r"(https?://)?(www\.)?(youtube|youtu|youtube-nocookie)\.(com|be)/"
        if re.search(youtube_regex, video_url):
            data['interactiveVideo']['video']['files'] = [{
                "path": video_url,
                "mime": "video/YouTube",
                "copyright": {
                    "license": "U"
                }
            }]
        else:
            data['interactiveVideo']['video']['files'] = [{
                "path": f"{user_h5p_folder}/videos/video.mp4",
                "mime": "video/mp4",
                "copyright": {
                    "license": "U"
                }
            }]
    else:
        data['interactiveVideo']['video']['files'] = [{
            "path": f"{user_h5p_folder}/videos/video.mp4",
            "mime": "video/mp4",
            "copyright": {
                "license": "U"
            }
        }]
    
    with open(json_file_path, 'w') as f:
        json.dump(data, f, indent=4)

def add_subtitles(json_file_path, languages, URL):
    with open(json_file_path, 'r') as f:
        data = json.load(f)
    
    data['interactiveVideo']['video']['textTracks']['videoTrack'] = []
    if not URL:
        for lang in languages:
            subtitle = {
                "label": f"Subtitles_{lang}",
                "kind": "subtitles",
                "srcLang": lang,
                "track": {
                    "path": f"subtitles_{lang}.vtt",
                    "mime": "text/plain",
                    "copyright": {
                        "license": "U"
                    }
                }
            }
            data['interactiveVideo']['video']['textTracks']['videoTrack'].append(subtitle)
    else:
        for lang in languages:
            subtitle = {
                "label": f"Subtitles_{lang}",
                "kind": "subtitles",
                "srcLang": lang,
                "track": {
                    "path": f"subtitles_{lang}.vtt",
                    "mime": "text/vtt",
                    "copyright": {
                        "license": "U"
                    }
                }
            }
            data['interactiveVideo']['video']['textTracks']['videoTrack'].append(subtitle)


    with open(json_file_path, 'w') as f:
        json.dump(data, f, indent=4)

def vtt_files_creation_with_video(audio_path: str, output_path: str, languages: list, vtt_file):
    model = WhisperModel.get_model("./base.pt")
    result = model.transcribe(audio_path)
    source_language = result['language']
    
    if source_language in languages:
        languages.remove(source_language)
    
    if vtt_file is None:
        source_vtt_path = os.path.join(output_path, f'subtitles_{source_language}.vtt')
        with open(source_vtt_path, "w") as f:
            f.write("WEBVTT\n\n")
            for idx, segment in enumerate(result['segments']):
                f.write(f"{format_timestamp(segment['start'])} --> {format_timestamp(segment['end'])}\n{segment['text']}\n\n")
        base_vtt_content = result['segments']
    else:
        base_vtt_path = os.path.join(output_path, f'subtitles_{source_language}.vtt')
        vtt_file.save(base_vtt_path)
        with open(base_vtt_path, "r") as f:
            base_vtt_content = f.readlines()

    for lang in languages:
        translated_vtt_path = os.path.join(output_path, f'subtitles_{lang}.vtt')
        with open(translated_vtt_path, "w") as f:
            if vtt_file is None:
                f.write("WEBVTT\n\n")
                for segment in base_vtt_content:
                    f.write(f"{format_timestamp(segment['start'])} --> {format_timestamp(segment['end'])}\n{translate_text(segment['text'], source_language, lang)}\n\n")
            else:
                for line in base_vtt_content:
                    if line in ['\n', '\r\n'] or any(substring in line for substring in ['WEBVTT', '-->']):
                        f.write(line)
                    else:
                        f.write(f"{translate_text(line.strip(), source_language, lang)}\n")

    languages.insert(0, source_language)

def vtt_files_creation_with_vtt(output_path: str, languages: list, vtt_file):
    vtt_file.save('/tmp/vtt_temp_file.vtt')
    
    source_language = detect_language(get_first_text_line_in_vtt('/tmp/vtt_temp_file.vtt'))
    base_vtt_path = os.path.join(output_path, f'subtitles_{source_language}.vtt')
    shutil.copy('/tmp/vtt_temp_file.vtt', base_vtt_path)
    
    print(source_language)
    if source_language in languages:
        languages.remove(source_language)
    
    with open(base_vtt_path, "r") as f:
        base_vtt_content = f.readlines()
        print(base_vtt_content)

    for lang in languages:
        translated_vtt_path = os.path.join(output_path, f'subtitles_{lang}.vtt')
        with open(translated_vtt_path, "w") as f:
            for line in base_vtt_content:
                if line in ['\n', '\r\n'] or any(substring in line for substring in ['WEBVTT', '-->']):
                    f.write(line)
                else:
                    f.write(f"{translate_text(line.strip(), source_language, lang)}\n")

def get_first_text_line_in_vtt(vtt_file):
    with open(vtt_file, 'r') as file:
        for line in file:
            if line not in ['\n', '\r\n'] and not any(substring in line for substring in ['WEBVTT', '-->']):
                return line.strip()
                    
def format_timestamp(seconds):
    millis = int((seconds - int(seconds)) * 1000)
    hours, remainder = divmod(int(seconds), 3600)
    minutes, seconds = divmod(remainder, 60)
    return f"{hours:02}:{minutes:02}:{seconds:02}.{millis:03}"

def find_previous_duration(liste, nombre):
    if liste[0] == nombre:
        return 0
    
    index = liste.index(nombre)
    
    for i in range(index - 1, -1, -1):
        if liste[i] != nombre:
            return liste[i]
    
    return 0

def generate_vtt_file(audio_path):
    model = WhisperModel.get_model("./base.pt")
    result = model.transcribe(audio_path)
    with open("/tmp/subtitle.vtt", "w") as f:
        f.write("WEBVTT\n\n")
        for idx, segment in enumerate(result['segments']):
            f.write(f"{format_timestamp(segment['start'])} --> {format_timestamp(segment['end'])}\n{segment['text']}\n\n")
    
    return "/tmp/subtitle.vtt"

def delete_files_in_folder(folder_path):
    for filename in os.listdir(folder_path):
        if filename == "content.json":
            continue
        file_path = os.path.join(folder_path, filename)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print(f"Failed to delete {file_path}. Reason: {e}")

def decale_nombres_egaux(liste):
    compteur = {}
    resultat = []

    for nombre_str in liste:
        nombre = int(nombre_str)
        if nombre in compteur:
            compteur[nombre] += 1
            nouveau_nombre = nombre - 0.01 * compteur[nombre]
        else:
            compteur[nombre] = 0
            nouveau_nombre = nombre

        resultat.append(nouveau_nombre)
    
    return resultat

def download_video_wget(video_url, output_path, output_name):
    full_output_path = f"{output_path}/{output_name}"
    try:
        subprocess.run(['wget', video_url, '-O', full_output_path], check=True)
        print(f"Video successfully downloaded to {full_output_path}")
        return True
    except subprocess.CalledProcessError as e:
        print(f"Error occurred while downloading the video: {str(e)}")
        return False