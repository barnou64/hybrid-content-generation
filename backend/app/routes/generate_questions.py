from flask import Blueprint, request, send_file, jsonify
from services.ai_services.generate_request import process_video_to_txt_and_csv
import os
import logging

generate_questions_bp = Blueprint('generate_questions', __name__)

@generate_questions_bp.route('/generate_questions', methods=['POST'])
def generate_questions():
    try:
        data_dir = "./ressources/data/"
        for f in os.listdir(data_dir):
            if f.endswith(".txt") or f.endswith(".csv"):
                os.remove(os.path.join(data_dir, f))

        if 'videoFile' not in request.files:
            return jsonify({'error': 'No file part'}), 400

        video_file = request.files['videoFile']
        if video_file.filename == '':
            return jsonify({'error': 'No selected file'}), 400

        video_name = request.form.get('videoName', "question")
        nb_question = int(request.form.get('number', 10))

        video_file_path = os.path.join('/tmp', video_name + '.mp4')
        video_file.save(video_file_path)

        process_video_to_txt_and_csv(video_file_path, video_name, video_name, nb_question)

        csv_file_path = f"./ressources/data/{video_name}.csv"
        if not os.path.exists(csv_file_path):
            return jsonify({'error': 'CSV file was not created.'}), 500

        return send_file(
            csv_file_path,
            as_attachment=True,
            download_name=video_name + ".csv",
            mimetype="text/csv"
        )
    except Exception as e:
        logging.error(f"Error occurred: {str(e)}")
        return jsonify({'error': str(e)}), 500