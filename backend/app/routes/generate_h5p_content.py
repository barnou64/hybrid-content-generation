from flask import Blueprint, request, send_file, jsonify
from services.h5p_services.h5p_generator_file import create_h5p_interactions_multiple_with_video_file
from services.h5p_services.h5p_generator_url import create_h5p_interactions_multiple_with_video_url
import json
import logging

generate_h5p_content_bp = Blueprint('generate_h5p_content', __name__)

@generate_h5p_content_bp.route('/generate_h5p_content', methods=['POST'])
def generate_h5p_content():
    try:
        video_name = request.form.get('videoName')
        data_file = request.files.get('dataFile')
        video_url = request.form.get('videoUrl', '')
        video_file = request.files.get('videoFile')
        vtt_file = request.files.get('vttFile')
        videoTime_str = request.form.get('videoTime')
        strict = request.form.get('STRICT') == 'true'
        languages = json.loads(request.form.get('languages', '[]'))

        videoTime = float(videoTime_str) if videoTime_str else None

        if video_url:
            zip_file_path = create_h5p_interactions_multiple_with_video_url(data_file, video_url, video_name + '.h5p', videoTime, strict=strict, languages=languages, vtt_file=vtt_file)
        elif video_file:
            zip_file_path = create_h5p_interactions_multiple_with_video_file(data_file, video_file, video_name + '.h5p', videoTime, strict=strict, languages=languages, vtt_file=vtt_file)
        else:
            return "Neither video URL nor video file provided", 400

        return send_file(zip_file_path, as_attachment=True, mimetype='application/zip', download_name=video_name + '.h5p')
    except Exception as e:
        logging.error(f"Error occurred: {str(e)}")
        return jsonify({'error': str(e)}), 500