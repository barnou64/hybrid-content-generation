from flask import Blueprint, request, send_file, jsonify
from utils.utils import extract_audio, generate_vtt_file
import logging

generate_vtt_bp = Blueprint('generate_vtt', __name__)

@generate_vtt_bp.route('/generator_vtt', methods=['POST'])
def generator_vtt():
    try:
        if 'videoFile' not in request.files:
            return jsonify({'error': 'No file part'}), 400

        video_file = request.files['videoFile']
        video_file_path = "/tmp/video-4-vtt.mp4"
        video_file.save(video_file_path)

        extract_audio(video_file_path, "/tmp/audio-4-vtt.mp3")
        vtt_file_path = generate_vtt_file("/tmp/audio-4-vtt.mp3")
        
        return send_file(
            vtt_file_path,
            as_attachment=True,
            download_name='subtitles.vtt',
            mimetype="text/vtt"
        )
    except Exception as e:
        logging.error(f"Error occurred: {str(e)}")
        return jsonify({'error': str(e)}), 500