from flask import Blueprint, jsonify
import psycopg2
from psycopg2 import sql
import os
import logging

delete_feedback_bp = Blueprint('delete_feedback', __name__)

@delete_feedback_bp.route('/clean/<int:id>', methods=['DELETE'])
def delete_feedback(id):
    connection_string = f"{os.getenv('POSTGRES_DRIVER')}://{os.getenv('POSTGRES_USER')}:{os.getenv('POSTGRES_PASSWORD')}@{os.getenv('POSTGRES_HOST')}:{os.getenv('POSTGRES_PORT')}/{os.getenv('POSTGRES_DB_NAME')}"
    try:
        with psycopg2.connect(connection_string) as conn:
            with conn.cursor() as cur:
                cur.execute("DELETE FROM feedback WHERE id = %s", (id,))
                conn.commit()
        return jsonify({"status": "success", "message": f"Feedback with ID {id} deleted"}), 200
    except Exception as e:
        logging.error(f"An error occurred: {e}")
        return jsonify({"error": "An internal error occurred"}), 500