from flask import Blueprint, request, jsonify
import psycopg2
from psycopg2 import sql, OperationalError
import os
import logging

feedback_bp = Blueprint('feedback', __name__)

@feedback_bp.route('/feedback', methods=['POST'])
def add_feedback():
    data = request.json
    if not data:
        return jsonify({"error": "Invalid request data, must be JSON format"}), 400

    required_fields = ['username', 'email', 'language', 'message']
    missing_fields = [field for field in required_fields if field not in data]
    if missing_fields:
        return jsonify({"error": f"Missing required fields: {', '.join(missing_fields)}"}), 400

    username = data.get('username')
    email = data.get('email')
    language = data.get('language')
    message = data.get('message')

    connection_string = f"postgresql://{os.getenv('POSTGRES_USER')}:{os.getenv('POSTGRES_PASSWORD')}@{os.getenv('POSTGRES_HOST')}:{os.getenv('POSTGRES_PORT')}/{os.getenv('POSTGRES_DB_NAME')}"
    try:
        with psycopg2.connect(connection_string) as conn:
            with conn.cursor() as cur:
                cur.execute(
                    sql.SQL("INSERT INTO feedback (username, email, language, message) VALUES (%s, %s, %s, %s)"),
                    [username, email, language, message]
                )
                conn.commit()
        return jsonify({"status": "success"}), 201
    except OperationalError as e:
        logging.error(f"Database connection failed: {e}")
        return jsonify({"error": "Database connection failed"}), 500
    except Exception as e:
        logging.error(f"An error occurred: {e}")
        return jsonify({"error": "An internal error occurred"}), 500