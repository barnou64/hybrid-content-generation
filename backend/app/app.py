from flask import Flask, request, send_file, jsonify, g
import json
from flask_socketio import SocketIO
from flask_cors import CORS
from flasgger import Swagger
from routes.delete_feedback import delete_feedback_bp
from routes.feedback import feedback_bp
from routes.generate_h5p_content import generate_h5p_content_bp
from routes.generate_questions import generate_questions_bp
from routes.generate_vtt import generate_vtt_bp
from dotenv import load_dotenv
import os
import time
import logging
from logging_config import setup_logging
import psycopg2
from psycopg2 import sql, OperationalError


# Charger les variables d'environnement à partir du fichier .env
load_dotenv()
POSTGRES_DRIVER = os.getenv('POSTGRES_DRIVER')
POSTGRES_HOST = os.getenv('POSTGRES_HOST')
POSTGRES_DB_NAME = os.getenv('POSTGRES_DB_NAME')
POSTGRES_USER = os.getenv('POSTGRES_USER')
POSTGRES_PASSWORD = os.getenv('POSTGRES_PASSWORD')
POSTGRES_PORT = os.getenv('POSTGRES_PORT')

app = Flask(__name__)
socketio = SocketIO(app)
CORS(app)
swagger = Swagger(app)

# Configure les logs
setup_logging()

@app.before_request
def start_timer():
    g.start = time.time()


def store_log_to_db(log_entry):
    connection_string = f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}:{POSTGRES_PORT}/{POSTGRES_DB_NAME}"
    try:
        with psycopg2.connect(connection_string) as conn:
            with conn.cursor() as cur:
                cur.execute(
                    sql.SQL("INSERT INTO logs (endpoint, method, duration, status_code, data) VALUES (%s, %s, %s, %s, %s)"),
                    [
                        log_entry.get('endpoint'),
                        log_entry.get('method'),
                        log_entry.get('duration'),
                        log_entry.get('status_code'),
                        json.dumps(log_entry.get('data'))
                    ]
                )
                conn.commit()
    except psycopg2.DatabaseError as e:
        logging.error(f"Database error occurred: {str(e)}")
    except Exception as e:
        logging.error(f"Error occurred while storing log: {str(e)}")

@app.after_request
def log_request(response):
    if hasattr(g, 'start'):
        duration = time.time() - g.start
        log_entry = {
            'endpoint': request.endpoint,
            'method': request.method,
            'duration': duration,
            'status_code': response.status_code,
            'data': request.get_json() if request.is_json else {}
        }
        store_log_to_db(log_entry)
    return response


@app.route('/')
def index():
  return jsonify({'message': 'Welcome to my Flask API!'})

app.register_blueprint(delete_feedback_bp)
app.register_blueprint(feedback_bp)
app.register_blueprint(generate_h5p_content_bp)
app.register_blueprint(generate_questions_bp)
app.register_blueprint(generate_vtt_bp)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
