import os
import requests
import json
from unidecode import unidecode
from flask import Flask, request, jsonify, send_file
from utils.utils import extract_audio, video_speech_to_text
import csv

app = Flask(__name__)

# Configuration
AUDIO_PATH = "./ressources/audios/audio.mp3"
MODEL_PATH = "./base.pt"
API_URL = "http://10.3.154.166:8080/api/generate"
HEADERS = {
    "Content-Type": "application/json",
    "User-Agent": "curl/7.68.0"
}

def generate_questions_and_answers(transcribed_text, nb_question):
    prompt = f"""
    Génère uniquement le contenu d'un fichier texte sans commentaires avant ni après, comprenant {nb_question} questions à choix multiples basées sur le texte suivant : {transcribed_text}. Pour chaque question, inclut 5 réponses possibles, en précisant si la réponse est fausse ou vraie. Utilise le format suivant pour les colonnes, en veillant à ce que les réponses soient pertinentes et cohérentes avec le texte fourni :

    Question [numéro de la question]:
    [Question]
    a) [Réponse 1] (f/v)
    b) [Réponse 2] (f/v)
    c) [Réponse 3] (f/v)
    d) [Réponse 4] (f/v)
    e) [Réponse 5] (f/v)

    Il faut donc un fichier texte avec les questions et les réponses."""

    payload = {
        "model": "llama3:instruct",
        "prompt": prompt,
        "stream": False
    }

    try:
        response = requests.post(API_URL, data=json.dumps(payload), headers=HEADERS)
        response.raise_for_status()
        return response.json().get("response", "")
    except requests.RequestException as e:
        print(f'An error occurred: {e}')
        return ""

def remove_special_characters(text):
    return unidecode(text)

def clean_quotes(text):
    return text.strip('"')

def save_txt(txt_content, file_path):
    cleaned_txt_content = remove_special_characters(txt_content)
    rows = [row for row in cleaned_txt_content.split('\n') if row.strip()]

    with open(file_path, 'w', encoding='utf-8') as txtfile:
        for row in rows:
            cleaned_row = clean_quotes(row)
            txtfile.write(cleaned_row + '\n')

def check_file_existence(file_path):
    print(file_path)
    full_path = os.path.abspath(file_path)
    print(full_path)
    if not os.path.exists(full_path):
        print(f"Erreur: le fichier {full_path} est introuvable.")
        return False
    return True

def parse_txt_to_csv(txt_file_path, csv_file_path):
    questions = []
    with open(txt_file_path, 'r', encoding='utf-8') as file:
        lines = file.readlines()

    current_question = {}
    for i in range(len(lines)):
        line = lines[i].strip()
        if line.startswith("Question "):
            if current_question:
                questions.append(current_question)
            current_question = {"answers": []}
            current_question["question"] = lines[i + 1].strip()
        elif line.startswith("Durée d'apparition :"):
            current_question["duration"] = line.split(":")[1].strip()
        elif line[0] in ['a', 'b', 'c', 'd', 'e']:
            answer, result = line[3:].rsplit('(', 1)
            result = result.replace(')', '').strip()
            current_question["answers"].append((answer.strip(), result))

    if current_question:
        questions.append(current_question)

    with open(csv_file_path, 'w', newline='', encoding='utf-8') as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(["question", "reponseA", "resultA", "reponseB", "resultB", "reponseC", "resultC", "reponseD", "resultD", "reponseE", "resultE", "duration", "renvoi"])
        for question in questions:
            row = [question["question"]]
            answers = question["answers"]
            for i in range(5):
                if i < len(answers):
                    row.append(answers[i][0])
                    row.append(answers[i][1])
                else:
                    row.append("")
                    row.append("")
            row.append(question.get("duration", ""))
            csv_writer.writerow(row)

def process_video_to_txt_and_csv(video_path, txt_file_name, csv_file_name, nb_question):
    txt_file_path = f"./ressources/data/{txt_file_name}.txt"
    csv_file_path = f"./ressources/data/{csv_file_name}.csv"
    print(video_path)

    if not check_file_existence(video_path):
        return
    # if not check_file_existence(AUDIO_PATH):
    #     return

    extract_audio(video_path, AUDIO_PATH)

    transcribed_text = video_speech_to_text(AUDIO_PATH, MODEL_PATH)
    txt_content = generate_questions_and_answers(transcribed_text, nb_question)
    save_txt(txt_content, txt_file_path)
    parse_txt_to_csv(txt_file_path, csv_file_path)

if __name__ == "__main__":
    app.run(debug=True)