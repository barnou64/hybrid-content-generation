import pandas as pd
import json
import shutil
import uuid
from utils.utils import *  # Assurez-vous que ce module est disponible et contient les fonctions nécessaires
from utils.helpers import process_interactive_video
from io import BytesIO
import os
from datetime import datetime

def create_h5p_interactions_multiple_with_video_file(data_file, video_file, h5p_file_name, videoTime, strict, languages=[], vtt_file=None):
    print("Début de la fonction create_h5p_interactions_multiple_with_video_file")
    
    current_time = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
    user_id = str(uuid.uuid4())
    user_h5p_folder = f'tmp/{current_time}'  
    os.makedirs(user_h5p_folder, exist_ok=True)

    original_h5p_folder = 'H5P'   
    shutil.copytree(original_h5p_folder, user_h5p_folder, dirs_exist_ok=True)

    if strict:
        autoContinue = False
        enableSolutionsButton = False
        requireCompletion = True
        allowMoveBackward = True
    else:
        autoContinue = True
        enableSolutionsButton = True
        requireCompletion = False
        allowMoveBackward = True

    print("Fin de creation de l'environnement")
    user_content_json_path = os.path.join(user_h5p_folder, 'content', 'content.json') 
    with open(user_content_json_path, 'r') as f:
        data = json.load(f)
    
    modify_video_restrictions(user_content_json_path, strict)
    data['interactiveVideo']['behaviour']['allowMoveBackward'] = allowMoveBackward 

    with open(user_content_json_path, 'w') as f:
        json.dump(data, f, indent=4)

    process_interactive_video(data_file, data, videoTime, decale_nombres_egaux, find_previous_duration, strict, autoContinue, allowMoveBackward, enableSolutionsButton, requireCompletion)

    os.makedirs(f'{user_h5p_folder}/videos', exist_ok=True)

    video_save_path = f'{user_h5p_folder}/videos/video.mp4'
    try:
        video_file.save(video_save_path)
        print(f"Vérification après sauvegarde : le fichier vidéo existe : {os.path.exists(video_save_path)}")
    except Exception as e:
        print(f"Erreur lors de la sauvegarde du fichier vidéo : {e} ")
    
    if not os.path.exists(video_save_path):
        raise FileNotFoundError(f"Le fichier vidéo n'a pas été sauvegardé à {video_save_path}")
    
    total_video_duration = video_duration(f'{user_h5p_folder}/videos/video.mp4')

    # Extraction et génération des fichiers VTT pour les langues spécifiées
    if languages:
        try: 
            video_path= f'{user_h5p_folder}/videos/video.mp4'
            if os.path.exists(video_path):
                audio_path = f'./ressources/audios/{user_id}.mp3'
                extract_audio(video_path, audio_path)
                output_path = f'{user_h5p_folder}/content'
                vtt_files_creation_with_video(audio_path, output_path, languages, vtt_file)
            else:
                raise FileNotFoundError(f"le fichier vidéo n'existe pas a l'emplacement {video_path} ")
        except Exception as e:
            print(f"Erreur lors de l'extraction de l'audio ou de la création des fichiers VTT : {e} ")
        
        URL = False
        add_subtitles(user_content_json_path, languages, URL)
        update_video_path(user_content_json_path, user_h5p_folder=user_h5p_folder)
    
    print("Fin de génération de fichiers VTT")

    print("Fin de creation du fichier json")

    h5p_file = zip_files_in_folder('./ressources/content/'f'{h5p_file_name}', user_h5p_folder)

    print("Fin de la fonction create_h5p_interactions_multiple_with_video_file")
    return h5p_file