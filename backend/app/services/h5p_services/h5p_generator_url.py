import pandas as pd
import json
import shutil
import uuid
from utils.utils import *  # Assurez-vous que ce module est disponible et contient les fonctions nécessaires
from utils.helpers import process_interactive_video
from io import BytesIO
import os
from datetime import datetime

def create_h5p_interactions_multiple_with_video_url(data_file, video_url, h5p_file_name, videoTime, strict, languages=[], vtt_file=None):
    print("Début de la fonction create_h5p_interactions_multiple_with_video_url")
    
    current_time = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
    user_id = str(uuid.uuid4())
    user_h5p_folder = f'tmp/{current_time}'  
    os.makedirs(user_h5p_folder, exist_ok=True)

    original_h5p_folder = 'H5P'   
    shutil.copytree(original_h5p_folder, user_h5p_folder, dirs_exist_ok=True)

    if strict:
        autoContinue = False
        enableSolutionsButton = False
        requireCompletion = True
        allowMoveBackward = True
    else:
        autoContinue = True
        enableSolutionsButton = True
        requireCompletion = False
        allowMoveBackward = True

    print("Fin de creation de l'environnement")

    user_content_json_path = os.path.join(user_h5p_folder, 'content', 'content.json') 
    with open(user_content_json_path, 'r') as f:
        data = json.load(f)
    
    modify_video_restrictions(user_content_json_path, strict)
    data['interactiveVideo']['behaviour']['allowMoveBackward'] = allowMoveBackward 

    with open(user_content_json_path, 'w') as f:
        json.dump(data, f, indent=4)

    process_interactive_video(data_file, data, videoTime, decale_nombres_egaux, find_previous_duration, strict, autoContinue, allowMoveBackward, enableSolutionsButton, requireCompletion)

    # Extraction et génération des fichiers VTT pour les langues spécifiées
    if languages:
        try: 
            download_video_wget(video_url, "tmp/", f'{user_id}.mp4')   
            output_path = f'{user_h5p_folder}/content'
            vtt_files_creation_with_video(output_path, languages, vtt_file=None)
        except Exception as e:
            print(f"Erreur lors de l'extraction de l'audio ou de la création des fichiers VTT : {e} ")
        URL = True
        add_subtitles(user_content_json_path, languages, URL)
        update_video_path(user_content_json_path, video_url)

    print("Fin de génération de fichiers VTT_video_url")

    print("Fin de creation du fichier json")

    h5p_file = zip_files_in_folder('./ressources/content/'f'{h5p_file_name}', user_h5p_folder)

    print("Fin de la fonction create_h5p_interactions_multiple_with_video_url")
    return h5p_file