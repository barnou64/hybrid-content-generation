import logging

def setup_logging():
    # Crée un gestionnaire de fichiers pour les logs, en mode 'w' pour effacer le contenu à chaque démarrage
    handler = logging.FileHandler('logs/api_logs.log', mode='w')
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    
    # Ajoute le gestionnaire au logger de l'application
    app_logger = logging.getLogger()
    app_logger.setLevel(logging.INFO)
    app_logger.addHandler(handler)