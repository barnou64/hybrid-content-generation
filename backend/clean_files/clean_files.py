import os
import time

def clean_old_files(directory, days=30):
    
    now = time.time()
    cutoff = now - (days * 86400)  
    for root, dirs, files in os.walk(directory):
        for file_name in files:
            file_path = os.path.join(root, file_name)
            if os.path.getmtime(file_path) < cutoff:
                print(f"Suppression du fichier: {file_path}")
                os.remove(file_path)

if __name__ == "__main__":
    directory = './ressources/content'  
    clean_old_files(directory, days=30)