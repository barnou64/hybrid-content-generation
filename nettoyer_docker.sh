#!/bin/bash

# Suppression de tous les conteneurs arrêtés
echo "Suppression de tous les conteneurs arrêtés..."
docker container prune -f

# Suppression de toutes les images non utilisées
echo "Suppression de toutes les images non utilisées..."
docker image prune -a -f

# Suppression de tous les volumes non utilisés
echo "Suppression de tous les volumes non utilisés..."
docker volume prune -f

# Suppression de tous les réseaux non utilisés
echo "Suppression de tous les réseaux non utilisés..."
docker network prune -f

# Optionnel: Suppression de tous les conteneurs, images, volumes, et réseaux
# echo "Suppression de tous les conteneurs, images, volumes, et réseaux..."
# docker system prune -a -f --volumes

echo "Nettoyage terminé."
