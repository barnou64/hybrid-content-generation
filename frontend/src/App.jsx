// App.jsx
import React, { useState, useEffect } from "react";
import { Navigation } from "./components/navigation";
import { Header } from "./components/header";
import { Home } from  './components/home';
import { MultipleChoiceQuestion } from "./components/multipleChoiceQuestion";
import { Tutoriel } from "./components/tutoriel";
import { QuestionGenerator } from "./components/questionGenerator";
import { Contact } from "./components/contact";
import { VttGenerator } from "./components/vttGenerator";
import InfoBox from './components/InfoBox'; // Assurez-vous que le chemin est correct
import JsonData from "./data/data.json";
import SmoothScroll from "smooth-scroll";
import "./App.css";

export const scroll = new SmoothScroll('a[href*="#"]', {
  speed: 1000,
  speedAsDuration: true,
});

const App = () => {
  const [landingPageData, setLandingPageData] = useState({});
  useEffect(() => {
    setLandingPageData(JsonData);
  }, []);

  return (
    <div>
      <Navigation />
      <Header data={landingPageData.Header} />
      <Home data={landingPageData.Home} />
      <MultipleChoiceQuestion data={landingPageData.MultipleChoiceQuestion} />
      <QuestionGenerator data={landingPageData.QuestionGenerator} />
      <VttGenerator data={landingPageData.VttGenerator} />
      <Tutoriel data={landingPageData.Tutoriel} />
      <Contact data={landingPageData.Contact} />
      <InfoBox data={landingPageData.InfoBox} />
    </div>
  );
};

export default App;
