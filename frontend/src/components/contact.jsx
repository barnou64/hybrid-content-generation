import React, { useState } from "react";

export const Contact = (props) => {
  const [formData, setFormData] = useState({
    username: "",
    email: "",
    language: "",
    message: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({ ...prevData, [name]: value }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log("Form submitted:", formData);

    // Envoyer les données à l'API
    try {
      const response = await fetch('http://localhost:8080/feedback', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });

      if (response.ok) {
        console.log('Data successfully sent to the API');
      } else {
        console.log('Error sending data to the API');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div>
      <div id="contact">
        <div className="container">
          <div className="row">
            <div className="col-md-8">
              <div className="section-title">
                <h2>Give us a feedback</h2>
                <p>
                  Please fill out the form below to send us your recommendations or any idea to enhance this application.
                </p>
                <form onSubmit={handleSubmit}>
                  <div className="form-group">
                    <label htmlFor="username">Name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="username"
                      name="username"
                      value={formData.username}
                      onChange={handleChange}
                      required
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <input
                      type="email"
                      className="form-control"
                      id="email"
                      name="email"
                      value={formData.email}
                      onChange={handleChange}
                      required
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="language">Language</label>
                    <input
                      type="text"
                      className="form-control"
                      id="language"
                      name="language"
                      value={formData.language}
                      onChange={handleChange}
                      required
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="message">Message</label>
                    <textarea
                      className="form-control"
                      id="message"
                      name="message"
                      value={formData.message}
                      onChange={handleChange}
                      required
                    ></textarea>
                  </div>
                  <button type="submit" className="btn btn-primary">
                    Submit
                  </button>
                </form>
              </div>
            </div>
            <div className="col-md-3 col-md-offset-1 contact-info">
              <div className="contact-item">
                <h3>Contact Info</h3>
                <p>
                  <span>
                    <i className="fa fa-map-marker"></i> Address
                  </span>
                  Collège STEE, Campus Montaury <br />
                  1 Allées du parc Montaury <br />
                  64600 Anglet, France
                </p>
              </div>
              <div className="contact-item">
                <p>
                  <span>
                    <i className="glyphicon glyphicon-cloud"></i> Affiliation
                  </span>
                  Université de Pau et des Pays de l'Adour,<br />
                  E2S-UPPA, LIUPPA
                </p>
              </div>
              <div className="contact-item">
                <p>
                  <span>
                    <i className="fa fa-envelope-o"></i> Email
                  </span>
                  nicolas.evain@univ-pau.fr
                </p>
              </div>
            </div>
          </div>
          <div className="row mt-4 justify-content-center">
            <div className="col-6 col-md-3 d-flex justify-content-center">
              <img src="/img/logo.png" alt="Logo 1" className="img-fluid logo-img" />
            </div>
            <div className="col-6 col-md-3 d-flex justify-content-center">
              <img src="/img/CU-Logo.png" alt="Logo 2" className="img-fluid logo-img" />
            </div>
            <div className="col-6 col-md-3 d-flex justify-content-center">
              <img src="/img/logo_unita.svg"/>
            </div>
            <div className="col-6 col-md-3 d-flex justify-content-center">
              <img src="/img/Logo_UPPA.svg"/>
            </div>
          </div>
        </div>
      </div>
      <style jsx>{`
        .contact-item p span {
          font-weight: bold;
        }
        .social ul {
          padding: 0;
          list-style: none;
        }
        .social ul li {
          display: inline;
          margin: 0 10px;
        }
        .logo-img {
          max-width: 100%;
          height: auto;
          margin-bottom: 20px;
        }
        @media (max-width: 768px) {
          .contact-item p {
            text-align: center;
          }
          .col-6 {
            display: flex;
            justify-content: center;
          }
        }
        .form-group {
          margin-bottom: 15px;
        }
        .form-control {
          width: 100%;
          border: 1px solid #ccc;
          border-radius: 4px;
        }
        .btn-primary {
          background-color: #007bff;
          border: none;
          padding: 10px 20px;
          color: white;
          cursor: pointer;
        }
      `}</style>
    </div>
  );
};
