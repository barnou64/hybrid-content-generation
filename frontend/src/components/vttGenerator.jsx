import React, { useState } from "react";

export const VttGenerator = () => {
  const [videoFile, setVideoFile] = useState(null);
  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    setIsSubmitting(true);

    const formData = new FormData();
    formData.append("videoFile", videoFile);

    fetch("http://localhost:8080/generator_vtt", {
      method: "POST",
      body: formData,
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(`HTTP error, status = ${response.status}`);
        }
        return response.blob();
      })
      .then((blob) => {
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement("a");
        a.style.display = "none";
        a.href = url;
        a.download = "subtitles.vtt"; // Fixed filename to subtitles.vtt
        document.body.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch((error) => {
        console.error("There was a problem with the fetch operation:", error.message);
      })
      .finally(() => {
        setIsSubmitting(false);
      });
  };

  return (
    <div id="vttGenerator" className="text-center">
      <hr className="custom-hr my-4" />
      <div className="container">
        <div className="col-md-8 col-md-offset-2 section-title">
          <h2>VTT Generator</h2>
          <p>Create your VTT file with subtitles.</p>
        </div>
        <form onSubmit={handleSubmit} encType="multipart/form-data">
          <div className="form-group">
            <input
              type="file"
              id="videoFile"
              name="videoFile"
              accept="video/*"
              required
              onChange={(e) => setVideoFile(e.target.files[0])}
              className="form-control"
            />
          </div>
          <button
            type="submit"
            className="btn btn-primary"
            disabled={isSubmitting}
            style={{ backgroundColor: isSubmitting ? 'grey' : 'blue' }}
          >
            {isSubmitting ? 'Processing...' : 'Generate VTT'}
          </button>
        </form>
      </div>
    </div>
  );
};
