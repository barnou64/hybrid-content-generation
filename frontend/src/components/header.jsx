import React from "react";

export const Header = (props) => {
  return (
    <header id="header">
      <div className="intro">
        <div className="overlay">
          <div className="container">
            <div className="intro-text text-center">
              <h1>Clevercontent</h1>
              <p>project</p> 
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};
