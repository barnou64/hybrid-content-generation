import React, { useState } from "react";

export const QuestionGenerator = () => {
  const [videoFile, setVideoFile] = useState(null);
  const [selectedNumber, setSelectedNumber] = useState(1); // New state for the selected number
  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    setIsSubmitting(true);

    const formData = new FormData();
    formData.append("videoFile", videoFile);
    formData.append("number", selectedNumber); // Append the selected number to formData

    fetch("http://localhost:8080/generate_questions", {
      method: "POST",
      body: formData,
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(`HTTP error, status = ${response.status}`);
        }
        return response.blob();
      })
      .then((blob) => {
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement("a");
        a.style.display = "none";
        a.href = url;
        a.download = `${"question"}.csv`; 
        document.body.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch((error) => {
        console.error("There was a problem with the fetch operation:", error.message);
      })
      .finally(() => {
        setIsSubmitting(false);
      });
  };

  return (
    <div id="questionGenerator" className="text-center">
      <div className="container">
        <div className="col-md-8 col-md-offset-2 section-title">
          <h2>Question Generator</h2>
          <p>Create your TXT file with questions.</p>
        </div>
        <form onSubmit={handleSubmit} encType="multipart/form-data">
          <div className="form-group">
            <input
              type="file"
              id="videoFile"
              name="videoFile"
              accept="video/*"
              required
              onChange={(e) => setVideoFile(e.target.files[0])}
              className="form-control"
            />
          </div>
          <div className="form-group">
            <label htmlFor="numberSelect">Select a number of question(s) (1-10):</label>
            <select
              id="numberSelect"
              value={selectedNumber}
              onChange={(e) => setSelectedNumber(e.target.value)}
              className="form-control"
              required
            >
              {[...Array(10)].map((_, index) => (
                <option key={index + 1} value={index + 1}>
                  {index + 1}
                </option>
              ))}
            </select>
          </div>
          <button
            type="submit"
            className="btn btn-primary"
            disabled={isSubmitting}
            style={{ backgroundColor: isSubmitting ? 'grey' : 'blue' }}
          >
            {isSubmitting ? 'Processing...' : 'Upload'}
          </button>
        </form>
      </div>
    </div>
  );
};
