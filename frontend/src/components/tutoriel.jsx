import React, { useState } from "react";

export const Tutoriel = (props) => {
  const [selectedService, setSelectedService] = useState(null);

  const tutorials = {
    service2: {
      title: "Tutorial for Service 2",
      content: (
        <>
          <div className="step mb-4">
            <h3>Hybrid content generator</h3>
            <br></br>
            <div className="responsive-iframe">
              <iframe
                src="https://dssdsd.h5p.com/content/1292321489504015787/embed"
                aria-label="tuto_hybrid_content"
                width="1088"
                height="637"
                frameBorder="0"
                allowFullScreen="allowfullscreen"
                allow="autoplay *; geolocation *; microphone *; camera *; midi *; encrypted-media *"
              ></iframe>
              <script
                src="https://dssdsd.h5p.com/js/h5p-resizer.js"
                charSet="UTF-8"
              ></script>
            </div>
          </div>
        </>
      ),
    },
    service3: {
      title: "Tutorial for Service 3",
      content: (
        <>
          <div className="step mb-4">
            <br></br>
            <iframe src="https://dssdsd.h5p.com/content/1292321508078055647/embed" aria-label="tuto_vtt_generator" width="1088" height="637" frameborder="0" allowfullscreen="allowfullscreen" allow="autoplay *; geolocation *; microphone *; camera *; midi *; encrypted-media *"></iframe>
            <script src="https://dssdsd.h5p.com/js/h5p-resizer.js" charset="UTF-8"></script><br></br>

            <p>Step 1: Go to the <a href="#questionGeneratorCSV">Question Generator CSV</a> page</p>
            <p>Step 2: Enter a name for your CSV file</p>
            <p>Step 3: Upload your video</p>
            <p>Step 4: Click the "Download" button to generate questions and answers in the .csv file with AI</p>
          </div>
        </>
      ),
    },
    service4: {
      title: "Tutorial for Service 4",
      content: (
        <>
          <div className="step mb-4">
            <h3>Upload the .h5p File to Moodle</h3><br></br>
            <iframe src="https://dssdsd.h5p.com/content/1292321515343620477/embed" aria-label="tuto_moodle" width="1088" height="637" frameborder="0" allowfullscreen="allowfullscreen" allow="autoplay *; geolocation *; microphone *; camera *; midi *; encrypted-media *"></iframe>
            <script src="https://dssdsd.h5p.com/js/h5p-resizer.js" charset="UTF-8"></script><br></br>
            <p>Step 1: Retrieve the .h5p file downloaded from our application</p>
            <p>Step 2: Log in to your Moodle course</p>
            <p>Step 3: Add a new activity</p>
            <p>Step 4: Select the blue H5P</p>
            <p>Step 5: Give a name to your video and upload your .h5p file in 'package file'</p>
            <p>Step 6: Go to the bottom of the page and click the 'Save and Return to Course' button</p>
            <video className="responsive-video" controls>
              <source src="path/to/your/video4.mp4" type="video/mp4" />
              Your browser does not support the video tag.
            </video>
          </div>
        </>
      ),
    },
  };

  const handleServiceSelect = (service) => {
    setSelectedService(service);
  };

  const tutorial = selectedService ? tutorials[selectedService] : null;

  return (
    <div id="tutoriel" className="container py-4">
      <br /><br />
      <hr className="custom-hr my-4" />
      <header className="bg-success text-white text-center py-4">
      </header>
      <main className="py-4">
        <section id="introduction">
          <h2 className="mb-4">Tutorial</h2>
          <h3>Choose a service:</h3>
          <ul className="button-list">
            <li><button className="service-button" onClick={() => handleServiceSelect('service2')}>Hybrid content generator</button></li>
            <li><button className="service-button" onClick={() => handleServiceSelect('service3')}>Generate VTT file</button></li>
            <li><button className="service-button" onClick={() => handleServiceSelect('service4')}>.h5P Upload Moodle</button></li>
          </ul>
        </section>
        {tutorial && (
          <>
            <section id="steps" className="mb-4">
              <h3 className="mb-4">Steps to use the application:</h3>
              {tutorial.content}
            </section>
            <section id="faq">
              <h2 className="mb-4">FAQ</h2>
              <div className="faq-item">
                <h3>How to contact support?</h3>
                <p>You can contact us via the contact page or send an email to alexandre.paboeuf@etud.univ-pau.fr</p>
                <h3>What is Strict mode?</h3>
                <p>Strict mode is designed so that students must watch the entire video and answer all questions. Therefore, navigating through the video is not allowed, and if a mistake is made on a question, the student is automatically taken back to the previous question.</p>
                <h3>What is the 'duration' column for?</h3>
                <p>The 'duration' column is important because it allows you to place your questions at the desired points in the video. For example: If my video is 60 seconds long and I have 2 questions to ask. I want my first question to be in the middle of my video, so I write '30' in the duration column for the first question. Then I want my second question to be at the very end of my video, so I leave the column EMPTY, and the question will automatically be at the end of the video.</p>
              </div>
            </section>
          </>
        )}
      </main>
      <footer className="bg-dark text-white text-center py-4">
        <p>&copy; 2024 Your Application. All rights reserved.</p>
      </footer>
      <style jsx>{`
        .button-list {
          display: flex;
          flex-wrap: wrap;
          justify-content: center;
          list-style: none;
          padding: 0;
        }
        .button-list li {
          margin: 10px;
        }
        .service-button {
          background-color: blue;
          color: white;
          padding: 10px 20px;
          border: none;
          border-radius: 5px;
          cursor: pointer;
          width: 100%;
          max-width: 200px;
        }
        .service-button:hover {
          background-color: darkblue;
        }
        .responsive-video, .responsive-iframe {
          width: 100%;
          height: auto;
          max-width: 100%;
          margin-top: 20px;
        }
        @media (max-width: 768px) {
          .service-button {
            padding: 8px 16px;
            font-size: 14px;
          }
        }
        @media (max-width: 576px) {
          .service-button {
            padding: 6px 12px;
            font-size: 12px;
          }
        }
      `}</style>
    </div>
  );
};
