// InfoBox.jsx
import React, { useState } from 'react';

const InfoBox = ({ message }) => {
  const [showInfo, setShowInfo] = useState(false);

  const toggleInfo = () => {
    setShowInfo(!showInfo);
  };

  return (
    <div style={{ display: 'inline-block', position: 'relative' }}>
      <span
        onClick={toggleInfo}
        style={{
          cursor: 'pointer',
          color: 'blue',
          marginLeft: '5px',
        }}
      >
        ?
      </span>
      {showInfo && (
        <div
          style={{
            position: 'absolute',
            top: '20px',
            left: '0',
            backgroundColor: '#f9f9f9',
            border: '1px solid #ccc',
            padding: '10px',
            zIndex: 1,
            width: '200px',
            boxShadow: '0 0 10px rgba(0, 0, 0, 0.1)',
          }}
        >
          {message}
        </div>
      )}
    </div>
  );
};

export default InfoBox; // Assurez-vous que le composant est exporté correctement ici
