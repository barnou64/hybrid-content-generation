import React, { useState } from "react";
import InfoBox from './InfoBox'; // Assurez-vous que le chemin est correct

export const MultipleChoiceQuestion = (props) => {
  const [videoName, setVideoName] = useState("");
  const [dataFile, setDataFile] = useState(null);
  const [videoFile, setVideoFile] = useState(null);
  const [vttFile, setVttFile] = useState(null);
  const [videoUrl, setVideoUrl] = useState("");
  const [videoTime, setVideoTime] = useState(""); // Nouvel état pour le temps de la vidéo
  const [strict, setStrict] = useState(false);
  const [selectedLanguages, setSelectedLanguages] = useState([]);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const dataFileUrl = './questions.xlsx';

  const languages = [
    { code: 'fr', name: 'French' },
    { code: 'en', name: 'English' },
    { code: 'pt', name: 'Portuguese' },
    { code: 'es', name: 'Spanish' },
    { code: 'it', name: 'Italian' },
    { code: 'de', name: 'German' },
    { code: 'uk', name: 'Ukrainian' },
    { code: 'ro', name: 'Romanian' },
  ];

  const toggleLanguageSelection = (code) => {
    setSelectedLanguages((prev) =>
      prev.includes(code)
        ? prev.filter((lang) => lang !== code)
        : [...prev, code]
    );
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setIsSubmitting(true);

    const formData = new FormData();
    formData.append("videoName", videoName);
    formData.append("dataFile", dataFile);
    formData.append("videoFile", videoFile);
    formData.append("vttFile", vttFile);
    formData.append("videoUrl", videoUrl);
    formData.append("videoTime", videoTime); // Ajout du temps de la vidéo à FormData
    formData.append("STRICT", strict);
    formData.append("languages", JSON.stringify(selectedLanguages));

    fetch("http://localhost:8080/generate_h5p_content", {
      method: "POST",
      body: formData,
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(`HTTP error, status = ${response.status}`);
        }
        return response.blob();
      })
      .then((blob) => {
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement("a");
        a.href = url;
        a.download = `${videoName}.h5p`;
        document.body.appendChild(a);
        a.click();
        a.remove();
        window.URL.revokeObjectURL(url);
      })
      .catch((error) => {
        console.error("There was a problem with the fetch operation:", error.message);
      })
      .finally(() => {
        setIsSubmitting(false);
      });
  };

  return (
    <div id="multipleChoiceQuestion" className="container">
      <hr className="custom-hr my-4" />
      <div className="container">
        <h2>Hybrid content generator</h2>
        <form onSubmit={handleSubmit} encType="multipart/form-data">
          <div className="overlay"></div>
          <div className="form-group">
            <label htmlFor="videoName">Video Name:</label>
            <input
              type="text"
              id="videoName"
              name="videoName"
              required
              placeholder="ex: VideoCours"
              value={videoName}
              onChange={(e) => setVideoName(e.target.value)}
              className="form-control"
            />
            <InfoBox message="Enter the name of your video." />
          </div>
          <div className="form-group">
            <label htmlFor="dataFile">Data File:</label>
            <div>
              <a href={dataFileUrl} download="questions.xlsx">
                Download Base Data File
              </a>
            </div>
            <input
              type="file"
              id="dataFile"
              name="dataFile"
              accept=".xlsx, .csv"
              onChange={(e) => setDataFile(e.target.files[0])}
              className="form-control"
            />
            <InfoBox message="Upload the data file containing the questions." />
          </div>
          <div className="form-group">
            <label htmlFor="videoFile">Video File:</label>
            <input
              type="file"
              id="videoFile"
              name="videoFile"
              accept=".mp4, .avi, .mov, .wmv, .flv, .mkv, .mpeg, .m4v, .webm, .3gp"
              onChange={(e) => setVideoFile(e.target.files[0])}
              className="form-control"
            />
            <InfoBox message="Upload the video file for the interactive session." />
          </div>
          <div className="form-group">
            <label htmlFor="vttFile">VTT File (Optional):</label>
            <input
              type="file"
              id="vttFile"
              name="vttFile"
              accept=".vtt"
              onChange={(e) => setVttFile(e.target.files[0])}
              className="form-control"
            />
            <InfoBox message="Upload the VTT file for subtitles, if available." />
          </div>
          <div className="form-group">
            <label htmlFor="videoUrl">Video URL:</label>
            <input
              type="url"
              id="videoUrl"
              name="videoUrl"
              placeholder="https://example.com/video.mp4"
              value={videoUrl}
              onChange={(e) => setVideoUrl(e.target.value)}
              className="form-control"
            />
            <InfoBox message="Provide the URL of the video if not uploading a file." />
            {videoUrl && (
              <div className="form-group">
                <label htmlFor="videoTime">Video Time (in seconds):</label>
                <input
                  type="number"
                  id="videoTime"
                  name="videoTime"
                  placeholder="e.g., 80"
                  value={videoTime}
                  onChange={(e) => setVideoTime(e.target.value)}
                  className="form-control"
                />
              </div>
            )}
          </div>
          <div className="form-group">
            <label htmlFor="strict">Strict:</label>
            <input
              type="checkbox"
              id="strict"
              name="strict"
              checked={strict}
              onChange={(e) => setStrict(e.target.checked)}
              className="form-check-input"
            />
            <InfoBox message="Enable strict mode to enforce answer correctness strictly." />
          </div>
          <div className="form-group">
            <InfoBox message="Select the languages for the subtitles." />
            <label>Languages:</label>
            <div className="language-list">
              {languages.map((language) => (
                <span
                  key={language.code}
                  onClick={() => toggleLanguageSelection(language.code)}
                  className={selectedLanguages.includes(language.code) ? 'selected' : ''}
                >
                  {language.name}
                </span>
              ))}
            </div>
          </div>
          <button
            type="submit"
            id="submitBtn"
            className="btn btn-primary"
            disabled={isSubmitting}
            style={{ backgroundColor: isSubmitting ? 'grey' : 'blue' }}
          >
            {isSubmitting ? 'Processing...' : 'Download'}
          </button>
        </form>
        <div className="overlay"></div>
      </div>
    </div>
  );
};
