from dataclasses import asdict
from streamlit_keycloak import login
import streamlit as st
import os
from dotenv import load_dotenv
from main_app.components.generate_h5p_content import generate_h5p_content
from main_app.components.generate_questions import generate_questions
from main_app.components.generate_vtt_file import generate_vtt_file
from main_app.components.tutoriel import tutorial
from main_app.components.contact import feedback
from main_app.components.home import home


def main():
    st.title(f"Welcome {keycloak.user_info['preferred_username']}!")
    #st.write(f"Here is your user information:")
    # st.write(asdict(keycloak))
    # Loading custom CSS
    with open('./main_app/styles/style.css') as f:
        css = f.read()


    st.markdown(f"""
        <style>
        {css}
        </style>
    """, unsafe_allow_html=True)

    st.sidebar.title("Navigation Menu")
    options = st.sidebar.radio("Select a page:", ["HOME", "HYBRID CONTENT GENERATOR", "QUESTION GENERATOR", "VTT GENERATOR", "TUTORIAL", "CONTACT"])

    if options == "HOME":
        home()
    elif options == "HYBRID CONTENT GENERATOR":
        generate_h5p_content()
        
    elif options == "QUESTION GENERATOR":
        generate_questions()

    elif options == "VTT GENERATOR":
        generate_vtt_file()

    elif options == "TUTORIAL":
        tutorial()
        
    elif options == "CONTACT":
        feedback()

load_dotenv()

api_ip = os.getenv("API_IP")
port = "8084"
        
#st.title("Streamlit Keycloak")
keycloak = login(
    url = f"{api_ip}:{port}",
    realm="hcg",
    client_id="streamlit",
)

if keycloak.authenticated:
    main()