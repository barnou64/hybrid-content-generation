import streamlit as st
import requests
from dotenv import load_dotenv
import os

load_dotenv()

def send_video_data(video_name, video_url=None, video_file=None, vtt_file=None, language="en"):
    api_url = "http://backend:8080/generate_h5p_content"
    form_data = {
        'videoName': video_name,
        'languages': f'["{language}"]',
    }
    files = {}

    if vtt_file is not None:
        files['vttFile'] = vtt_file

    if video_url is not None:
        form_data['videoUrl'] = video_url

    if video_file is not None:
        files['videoFile'] = video_file

    try:
        response = requests.post(api_url, data=form_data, files=files)
        if response.status_code == 200:
            st.success("Content generated successfully!")
            h5p_filename = f"{video_name}.h5p"
            
            with open(h5p_filename, "wb") as f:
                f.write(response.content)
            st.download_button(
                label="Download H5P File",
                data=response.content,
                file_name=h5p_filename,
                mime="application/octet-stream"
            )
        else:
            st.error(f"Failed to generate content: {response.status_code} - {response.text}")
    except Exception as e:
        st.error(f"An error occurred: {str(e)}")

def generate_questions_call(video_file, number_of_questions):
    api_url = "http://backend:8080/generate_questions"
    form_data = {
        'number': number_of_questions,
    }
    files = {'videoFile': video_file}

    try:
        response = requests.post(api_url, data=form_data, files=files)
        if response.status_code == 200:
            with open("generated_questions.csv", "wb") as f:
                f.write(response.content)
            st.success("Questions generated successfully!")
            st.download_button(
                label="Download CSV",
                data=response.content,
                file_name="generated_questions.csv",
                mime="text/csv"
            )
        else:
            st.error(f"Failed to generate questions: {response.status_code} - {response.text}")
    except Exception as e:
        st.error(f"An error occurred: {str(e)}")

def generate_vtt_call(video_file):
    api_url = "http://backend:8080/generator_vtt"
    files = {'videoFile': video_file}

    try:
        response = requests.post(api_url, files=files)
        if response.status_code == 200:
            with open("subtitles.vtt", "wb") as f:
                f.write(response.content)
            st.success("VTT file generated successfully!")
            st.download_button(
                label="Download VTT",
                data=response.content,
                file_name="subtitles.vtt",
                mime="text/vtt"
            )
        else:
            st.error(f"Failed to generate VTT: {response.status_code} - {response.text}")
    except Exception as e:
        st.error(f"An error occurred: {str(e)}")

def send_feedback_call(name, email, language, message):
    api_url = "http://backend:8080/feedback"
    feedback_data = {
        "username": name,
        "email": email,
        "language": language,
        "message": message
    }
    try:
        response = requests.post(api_url, json=feedback_data)
        if response.status_code == 201:
            st.success("Feedback received successfully!")
        else:
            st.error(f"Failed to submit feedback: {response.status_code} - {response.text}")
    except Exception as e:
        st.error(f"An error occurred: {str(e)}")