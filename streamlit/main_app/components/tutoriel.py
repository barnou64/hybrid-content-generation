import streamlit as st

def tutorial():
    st.subheader("Tutorial")
    st.write("Choose a service:")
        
    col1, col2, col3 = st.columns(3)
        
    with col1:
        if st.button("Hybrid content generator"):
            st.write("Hybrid content generator selected.")
                
    with col2:
        if st.button("Generate VTT file"):
            st.write("Generate VTT file selected.")
        
    with col3:
        if st.button(".h5P Upload Moodle"):
            st.write(".h5P Upload Moodle selected.")