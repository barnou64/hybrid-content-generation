import streamlit as st
from main_app.services.api_call import send_video_data

def generate_h5p_content():
    st.subheader("CONTENT GENERATOR")
    option = st.radio("Please choose an input type:", ["Video", "URL"])
    
    video_name = st.text_input("Video name", placeholder="e.g., LectureVideo")
    vtt_file = st.file_uploader("Upload VTT file (Optional)", type=["vtt"])
    language = st.radio("Select language", options=["English", "French", "Spanish", "German"])

    video_file = None
    video_url = None
    if option == "Video":
        st.subheader("Selected Video")
        video_file = st.file_uploader("Upload video file", type=["mp4", "mpeg4"])
    elif option == "URL":
        st.subheader("Selected URL")
        video_url = st.text_input("Enter video URL", placeholder="https://example.com/video.mp4")

    if st.button("Generate Content"):
        send_video_data(video_name, video_url=video_url, video_file=video_file, vtt_file=vtt_file, language=language)