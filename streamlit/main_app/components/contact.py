import streamlit as st
from main_app.services.api_call import send_feedback_call  # Assurez-vous que ce chemin est correct

def feedback():
    st.subheader("Contact us")
    st.write("Please fill out the form below to send us your recommendations or any idea to enhance this application.")
    
    with st.form("feedback_form"):
        name = st.text_input("Name")
        email = st.text_input("Email")
        language = st.text_input("Language")
        message = st.text_area("Message")
        submit_button = st.form_submit_button(label="Submit")
    
    if submit_button:
        if name and email and language and message:
            send_feedback_call(name, email, language, message)
        else:
            st.warning("Please complete all fields before submitting.")
    
    st.sidebar.header("Contact Info")
    st.sidebar.write("📍 **Address**")
    st.sidebar.write("College STEE, Montaury Campus")
    st.sidebar.write("1 Allées du parc Montaury,")
    st.sidebar.write("64600 Anglet, France")
    
    st.sidebar.write("🌐 **Affiliation**")
    st.sidebar.write("University of Pau and Pays de l'Adour,")
    st.sidebar.write("E2S-UPPA, LIUPPA")
    
    st.sidebar.write("📧 **Email**")
    st.sidebar.write("nicolas.evain@univ-pau.fr")