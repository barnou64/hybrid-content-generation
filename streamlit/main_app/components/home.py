import streamlit as st

def home():
    st.header("CLEVERCONTENT")
    st.subheader("Create Interactive and Augmented Learning Objects")
    st.write("""
            This project is part of Connect-UNITA, a European initiative led by the University of Pau and Pays de l'Adour (UPPA).
            The main objective is to develop an innovative application that automates the creation of interactive and augmented 
            educational resources using artificial intelligence. In other words, it helps teachers create interactive videos 
            that integrate questions and subtitles for their flipped classrooms. These resources will be integrated into Learning 
            Management Systems (LMS) to enhance the analysis of learning data and promote pedagogical innovation, revolutionizing 
            teaching methods in line with contemporary higher education requirements.
        """)
