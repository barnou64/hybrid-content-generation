import streamlit as st
from main_app.services.api_call import generate_vtt_call  

def generate_vtt_file():
    st.subheader("VTT File Generator")
    
    col1, col2 = st.columns([1, 1])
    with col1:
        st.write("Create your VTT file")
    with col2:
        create_file = st.file_uploader("Choose file", type=["mp4", "mpeg4"])

    if st.button("Generate VTT File"):
        if create_file is not None:
            generate_vtt_call(create_file)
        else:
            st.warning("Please upload a video file.")