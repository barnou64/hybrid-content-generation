import streamlit as st
from main_app.services.api_call import generate_questions_call


def generate_questions():
    st.subheader("Create your text file with questions")

    col1, col2 = st.columns([1, 2])
    with col1:
        st.write("Text file")
        st.write("Select number")

    with col2:
        choose_file = st.file_uploader("Upload video file", type=["mp4", "mpeg4"])
        select_number = st.text_input("e.g., Number of questions")

    if st.button("Generate Questions"):
        if choose_file is not None and select_number:
            generate_questions_call(choose_file, select_number)
        else:
            st.warning("Please upload a text file and enter a number of questions.")