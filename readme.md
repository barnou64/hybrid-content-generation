1) Ecrire le contexte et la problèmatique de la présentation que je t'ai faite
2) Comprendre et me présenter le projet h5Phcg --> documentation
3) Installer un Mediawiki en local avec docker sur ta machine et essayer de comprendre comment sa fonctionne en créant une page sur ton wiki.
4) Installer un moodle en local avec docker puis créer un objet h5p avecc hybrid content generator et l'upload sur moodle pour observer le résultat.
Pour le 6 septembre

Télécharger le model whsiper (base.pt)

attention il faut se trouver dans /root/hybrid-content-generation

docker-compose down
docker-compose up -d

docker ps
pour les logs du server web : 
docker logs hybrid-content-generation_nginx_1

pour les logs du server python : 
docker logs hybrid-content-generation_python-api_1

commande docker pour controler la stack :
docker ps -a 
docker rm ID_CONTAINER

docker images
docker rmi ID_IMAGE (-f) pour forcer

docker network ls

docker volume ls

voir les images : docker images
voir les containers : docker ps -a
supprimer des containers : docker rm 
supprimer les images : docker rmi
voir les volumes : docker volume ls
lancer les containers : docker-compose up -d
arreter les containers : docker-compose down
voir les logs : docker logs hybrid-content-generation_python-api_1 -f


video H5P https://h5p.com/

pip install ffmpeg-python

RUN apt-get update && apt-get upgrade -y && pip install flask flask-cors youtube-dl && apt-get install -y zip ffmpeg && pip install moviepy && pip install openai && pip install pydub && pip install speechrecognition && pip install translate && pip install --upgrade pip && pip install --no-cache-dir -Ur requirements.txt

// 
import re
from unidecode import unidecode

def remove_special_characters(text):
    # Convertir les caractères accentués en équivalents non accentués
    text = unidecode(text)
    # Expression régulière pour conserver uniquement les caractères alphanumériques et les espaces
    cleaned_text = re.sub(r'[^A-Za-z0-9\s]', '', text)
    return cleaned_text

text = "Héllo, Wörld! Thîs îs àn éxamplé wîth spécîâl chärâctérs: @#$%^&*()"
cleaned_text = remove_special_characters(text)
print(cleaned_text)
//



## Route /generate_video

The /generate_video route is responsible for handling the upload and processing of a video file. 
Below is an explanation of how this route works and the key functions it utilizes:

### Endpoint Details

    - URL: localhost:8081/generate_video
    - Method: POST
    - Description: Uploads a video file, processes it to generate a CSV file containing questions and answers based on the video content, and returns the generated CSV file.

### Parameters

    - videoFile (file): The video file to be uploaded.
    - videoName (string, optional): The name of the video. If not provided, the name is derived from the uploaded video file's name.

### Functionality

 1 Clean Existing CSV Files:
Before processing a new video, the route deletes all existing CSV files in the ./ressources/data/ directory to ensure that old data does not interfere with the new upload.

```
csv_dir = "./ressources/data/"
for f in os.listdir(csv_dir):
    if f.endswith(".csv"):
        os.remove(os.path.join(csv_dir, f))```

 2 Check for Video File in Request:
The route checks if the videoFile is present in the request. If not, it returns an error message.

```
if 'videoFile' not in request.files:
    return jsonify({'error': 'No file part'}), 400```

 3 Save the Uploaded Video:
The uploaded video file is saved to the /tmp directory. If no video name is provided, it defaults to the name derived from the uploaded file.
```
video_file = request.files['videoFile']
if video_file.filename == '':
    return jsonify({'error': 'No selected file'}), 400

video_name = request.form.get('videoName', os.path.splitext(video_file.filename)[0])
video_file_path = os.path.join('/tmp', video_name + '.mp4')
video_file.save(video_file_path)
```

 4 Process the Video:
The saved video is processed to generate a CSV file containing questions and answers. The process_video_to_csv function is called for this purpose.

```
try:
    process_video_to_csv(video_file_path, video_name)  # Call the function to process the video and generate the CSV file
except Exception as e:
    return jsonify({'error': str(e)}), 500

```
 5 Return the Generated CSV File:
The generated CSV file is located in the ./ressources/data/ directory and is sent back as a response.

```
csv_file_path = f"./ressources/data/{video_name}.csv"
if not os.path.exists(csv_file_path):
    return jsonify({'error': 'CSV file was not created.'}), 500

return send_file(
    csv_file_path,
    as_attachment=True,
    download_name=video_name + ".csv",  # Use the retrieved name for the CSV file
    mimetype="text/csv"
)
```

### Example Request
Here is an example of how to send a request to the /generate_video endpoint using curl:
```
bash
Copier le code
curl -X POST http://localhost:8080/generate_video \
    -F "videoFile=@path_to_your_video_file.mp4" \
    -F "videoName=optional_video_name"
```

## Supporting Functions

**process_video_to_csv:**
This function is responsible for extracting audio from the video, converting the audio format, transcribing the audio to text, generating questions and answers, and saving the result as a CSV file. It involves multiple steps and utilizes other helper functions such as **extract_audio**, **convert_to_mono_pcm**, and **video_speech_to_text**.

### Error Handling
The route includes basic error handling to ensure that appropriate error messages are returned if something goes wrong during the file upload or processing steps.

## Docker Commands
To run and manage the Docker containers for this project, the following commands are useful:

- Start and Stop Containers:
```
docker-compose down  # Stop all containers
docker-compose up -d  # Start all containers in detached mode
```

- Check Running Containers:
```
docker ps  # List all running containers
```

- View Logs:
```
docker logs hybrid-content-generation_python-api_1  # View logs for the Python API server
docker logs hybrid-content-generation_nginx_1  # View logs for the web server
```

- Remove Containers and Images:
```
docker ps -a  # List all containers (including stopped ones)
docker rm ID_CONTAINER  # Remove a container
docker images  # List all Docker images
docker rmi ID_IMAGE  # Remove a Docker image
```

- Network and Volume Management:
```
docker network ls  # List Docker networks
docker volume ls  # List Docker volumes
```
These commands are essential for managing the Docker environment where the Flask application runs. They help in starting, stopping, and debugging the application efficiently.


## Interactive Video Simple Choice Question Route
This route is used to create an interactive video with simple choice questions in H5P format. It takes a CSV or Excel file containing questions and their associated metadata and generates an H5P package that can be uploaded to an H5P-compatible platform.

### Endpoint

**/interactive_video_simple_choice_question**

### Method
**POST**

### Request Parameters

 - **data_file_path**: The path to the CSV or Excel file containing the questions and their associated metadata.
 - **video_file_path**: The path to the video file that will be used in the H5P package.
 - **h5p_file_name**: The name of the resulting H5P file.
 - **strict**: A boolean flag indicating whether to apply strict content restrictions.
 - **languages**: A list of languages for which subtitles should be generated (optional).
 - **subtitles**: A list of subtitle files to be included in the H5P package (optional).

### Data File Format
The data file (CSV or Excel) should contain the following columns:

- **question**: The text of the question.
- **duration**: The duration (in seconds) for which the question will be displayed (optional). If not provided, the duration will be evenly distributed.
- **options**: The possible choices for the question, separated by a delimiter.
- **correct_option**: The correct choice from the options.
An example of the data file format can be found at:
```
backend/app/resources/data/question.xls
```
##  Usage Example 

Here are example usages of this endpoint using React:
```
fetch("http://localhost:8080/interactive_video_simple_choice_question", {
      method: "POST",
      body: formData,
    })
```